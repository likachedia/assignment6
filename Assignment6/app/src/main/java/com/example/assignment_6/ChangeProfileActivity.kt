package com.example.assignment_6

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.forEach
import com.example.assignment_6.databinding.ActivityChangeProfileBinding

class ChangeProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChangeProfileBinding
    private lateinit var efirstName: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangeProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        listeners()
        binding.btnSave.setOnClickListener {
            checkFields()
        }
    }

    private fun init() {
        val email:String = intent.extras?.getString("emailFrom", "email") ?: ""
        val firstN:String = intent.extras?.getString("firstNameFrom", "first name") ?: ""
        val lastN:String = intent.extras?.getString("lastNameFrom", "last name") ?: ""
        val yearB:String = intent.extras?.getString("yearFrom", "year") ?: ""
        val gender:String = intent.extras?.getString("genderFrom", "gender") ?: ""

        binding.eEmail.hint = email
        binding.efirstName.hint = firstN
        binding.eLastName.hint = lastN
        binding.eYear.hint = yearB
        binding.eGender.hint = gender
    }

    private fun  back() {
        efirstName = binding.efirstName.text.toString()
        var elastName = binding.eLastName.text.toString()
        var eEmail = binding.eEmail.text.toString()
        var eYearOfBirth = binding.eYear.text.toString()
        var eGender = binding.eGender.text.toString()
        val intent = intent
        intent.putExtra("firstName", efirstName)
        intent.putExtra("lastName", elastName)
        intent.putExtra("email", eEmail)
        intent.putExtra("year", eYearOfBirth)
        intent.putExtra("gender", eGender)
        setResult(RESULT_OK, intent)
        finish()
    }
    private fun listeners() {
        binding.root.forEach {
            it.setOnKeyListener{
                    view, keyCode, _ -> handleKeyEvent(view, keyCode)
            }
        }
    }

    private fun checkEmail(email: String): Boolean {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true
        }
        Toast.makeText(applicationContext, getString(R.string.invalid_email), Toast.LENGTH_SHORT).show()
        return false
    }
    private fun checkFields() {
        if (binding.eEmail.text.isNullOrEmpty() || binding.efirstName.text.isNullOrEmpty()
            || binding.eLastName.text.isNullOrEmpty() || binding.eYear.text.isNullOrEmpty()
            || binding.eGender.text.isNullOrEmpty()) {
            Toast.makeText(applicationContext, getString(R.string.field_is_empty), Toast.LENGTH_SHORT).show()
            return
        }
        val email = binding.eEmail.text.toString()
        if (!checkEmail(email)) {
            return
        }
        back()
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}