package com.example.assignment_6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log.d
import androidx.activity.result.contract.ActivityResultContracts
import com.example.assignment_6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
   // private lateinit var email: Editable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
      //  email = binding.tvEmail.text!!
        setContentView(binding.root)
        binding.btnChange.setOnClickListener {
           change()
        }

    }

   private fun change() {
        val intent = Intent(this, ChangeProfileActivity::class.java)
        intent.putExtra("firstNameFrom", binding.tvfirstName.text.toString())
       intent.putExtra("lastNameFrom", binding.tvLastName.text.toString() )
       intent.putExtra("emailFrom", binding.tvEmail.text.toString() )
       intent.putExtra("yearFrom", binding.tvYear.text.toString() )
       intent.putExtra("genderFrom", binding.tvGender.text.toString() )
        //startActivity(intent)
        changedText.launch(intent)
    }

    private val changedText =    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        // it.data.extras.getString("")
        if(it.resultCode == RESULT_OK) {
            val changedFN = it.data?.extras?.getString("firstName", "firstName")
            val changedLN = it.data?.extras?.getString("lastName", "lastname") ?: "some text"
            val changedEmail = it.data?.extras?.getString("email", "email") ?: "some text"
            val changedYear = it.data?.extras?.getString("year", "year") ?: "some text"
            val changedGender = it.data?.extras?.getString("gender", "gender") ?: "some text"

            binding.tvfirstName.text = changedFN
            binding.tvLastName.text = changedLN
            binding.tvEmail.text = changedEmail
            binding.tvYear.text = changedYear
            binding.tvGender.text = changedGender
        }
    }
}